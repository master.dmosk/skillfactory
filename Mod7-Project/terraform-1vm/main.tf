terraform {
  required_version = "= 1.1.7"

  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "= 0.73"
    }
  }
}

provider "yandex" {
  token     = "..."
  cloud_id  = "..."
  folder_id = var.yandex_folder_id
  zone      = "ru-central1-a"
}
