terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.73"
}

provider "yandex" {
  token     = "..."
  cloud_id  = "..."
  folder_id = var.yandex_folder_id
  zone      = "ru-central1-a"
}
